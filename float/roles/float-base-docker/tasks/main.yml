---

- set_fact:
    container_runtime: "podman"

- include_tasks: docker.yml
  when: "container_runtime == 'docker'"

- include_tasks: podman.yml
  when: "container_runtime == 'podman'"

- name: Login to the Docker registry
  shell: 'echo -n "{{ docker_registry_password }}" | {{ container_runtime }} login -u "{{ docker_registry_username }}" --password-stdin "{{ docker_registry_url }}"'
  changed_when: False
  when: "docker_registry_url != ''"

- name: Create docker scripts dir
  file:
    path: /usr/lib/float/docker
    state: directory

- name: Install float-pull-image script
  template:
    src: float-pull-image.j2
    dest: /usr/sbin/float-pull-image
    mode: 0755

- include_tasks: start.yml

# Grab the list of currently running containers, and stop / cleanup
# the ones that are not specified in services.yml.

- name: Get list of running containers
  shell: "{{ container_runtime }} ps --format={% raw %}'{{.Names}}'{% endraw %}"
  changed_when: false
  check_mode: no
  register: docker_running_containers
  ignore_errors: true

- set_fact:
    enabled_container_tags: "{{ float_enabled_containers | map(attribute='tag') | list }}"

- systemd:
    name: "docker-{{ item }}"
    state: stopped
    enabled: no
  with_items:
    - "{{ docker_running_containers.stdout_lines }}"
  when: "item not in enabled_container_tags"
  ignore_errors: true

- file:
    path: "/var/lib/prometheus/node-exporter/container-{{ item }}.prom"
    state: absent
  with_items:
    - "{{ docker_running_containers.stdout_lines }}"
  when: "docker_running_containers is succeeded and item not in enabled_container_tags"
  ignore_errors: true

- name: Install docker cleanup script
  template:
    src: cleanup.sh.j2
    dest: /usr/local/bin/docker-cleanup
    mode: 0755

- name: Install docker cleanup cron job
  copy:
    dest: /etc/cron.d/docker-cleanup
    content: "33 */3 * * * root runcron --quiet /usr/local/bin/docker-cleanup\n"
    mode: 0644

- name: Install in-container script
  template:
    src: in-container.j2
    dest: /usr/local/bin/in-container
    mode: 0755
